package com.example.colorgridrepohomework.views

data class ColorState(
    var colorList: MutableList<String> = mutableListOf(),
    var isLoading: Boolean = false,
    var isRaveMode: Boolean = false
)