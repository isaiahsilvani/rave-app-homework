package com.example.colorgridrepohomework.views

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import com.example.colorgridrepohomework.R
import com.example.colorgridrepohomework.databinding.ActivityColorBinding
import com.example.colorgridrepohomework.viewModel.ColorGridViewModel

class ColorActivity : AppCompatActivity() {
    val TAG = "ColorActivity"

    lateinit var binding: ActivityColorBinding
    lateinit var colorState: ColorState
    private val viewModel by viewModels<ColorGridViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityColorBinding.inflate(layoutInflater)
        initObservers()
        initViews()
        setContentView(binding.root)
    }

    private fun initViews() {
        with(binding) {
            button.setOnClickListener {
                viewModel.generateRandomColors()
            }
            raveModeBtn.setOnClickListener {
                viewModel.generateRaveMode()
            }

        }
    }

    private fun initObservers() {
        // viewModel.optionsState.observe(viewLifecycleOwner) { theState ->
        with(binding) {
            viewModel.colorState.observe(this@ColorActivity) { theState ->
                // List of views in our activity
                val cells = listOf(textView, textView2, textView3, textView4, textView5, textView6,
                    textView7, textView8, textView9)

                with(theState) {
                    fun setRandomColors() {
                        for (i: Int in cells.indices) {
                            val cell = cells[i]
                            val color = colorList[i]
                            cell.setBackgroundColor(Color.parseColor(color))
                        }
                    }
                    // Set random colors UI once
                    if (colorList.size > 0) {
                        setRandomColors()
                    }

                    // Loading UI
                    if (isLoading) {
                        textView10.text = "WE'RE LOADING A PARTY!!!"
                    } else {
                        textView10.text = "RAVE APP BY ISAIAH"
                    }
                    // RAVE MODE UI
                    if (isRaveMode) {
                        raveModeBtn.text = "RAVE MODE ON"
                        textView10.text = ""
                        button.setBackgroundColor(Color.GRAY)
                    } else {
                        raveModeBtn.text = "RAVE MODE OFF"
                        button.setBackgroundColor(Color.WHITE)
                    }
                }

            }
        }

    }
}