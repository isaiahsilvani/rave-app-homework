package com.example.colorgridrepohomework.model.repository

// You can create a single instance of an object BY LITERALLY DECLARING AN OBJECT
// A class would just be instansiated over and over again when you call it,
// and would bloat up the memory pretty quickly.
object ColorRepo {
    val TAG = "ColorGridRepo"

    val colorHexOptions = setOf(
        "#34568B",
        "#FF6F61",
        "#6B5B95",
        "#88B04B",
        "#F7CAC9",
        "#92A8D1",
        "#955251",
        "#B565A7",
        "#009B77",
        "#DD4124",
        "#D65076",
        "#45B8AC",
        "#EFC050",
        "#5B5EA6",
        "#9B2335",
        "#DFCFBE",
        "#55B4B0",
        "#E15D44",
        "#7FCDCD",
        "#BC243C"
    )

 /*

  ALTERNATIVELY.......

  Steps:
     - Create a private constructor for your class
     - create a private, static istance variable to hold
        the single instance of the class in existence
     - create a 'getInstance' function that will check to see if the instance is null.
     if its null, call the constructot with teh appropriate parameters, if it is not nul
     then return the instance that is there.


  companion object {
        // SO OUR VIEW MODEL CAN CREATE A SINGLE INSTANCE OF THE COLOR REPO
        // SO WE CAN MESS WITH THE DATA
        private var instance: ColorRepo? = null
        fun getInstance(baseColor: String): ColorRepo =
            instance ?: ColorRepo(baseColor).apply {
                instance = this
            }

  */
    }