package com.example.colorgridrepohomework.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.colorgridrepohomework.model.repository.ColorRepo
import com.example.colorgridrepohomework.views.ColorState
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class ColorGridViewModel: ViewModel() {

    val TAG = "ColorGridViewModel"

    private val repo: ColorRepo = ColorRepo
    // Create a single instance of the ColorRepo class
    private var _colorState: MutableLiveData<ColorState> = MutableLiveData(ColorState())
    val colorState: LiveData<ColorState> get() = _colorState



    fun generateRandomColors() {
        // IF RAVE MODE IS OFF, GENERATE RANDOM COLORS ONCE
        if (_colorState.value?.isRaveMode == false) {
            val newColors = getRandomColors()
            viewModelScope.launch {
                with(_colorState) {
                    value = value?.copy(isLoading = true)
                    delay(2000)
                    value = value?.copy(colorList = newColors)
                    value = value?.copy(isLoading = false)
                }
            }
        }
    }
    // GENERATE RANDOM NUMBERS FROM REPO!!!
    private fun getRandomColors(): MutableList<String> {
        var newColorList: MutableList<String> = mutableListOf()
        for (i: Int in 0..8) {
            newColorList.add(repo.colorHexOptions.random())
        }
        return newColorList
    }

    // CONTINUOUSLY GENERATE COLORS
    fun generateRaveMode() {
        viewModelScope.launch {
            with (_colorState) {
                if (value?.isRaveMode == false) {
                    value = value?.copy(isRaveMode = true)
                    while (value?.isRaveMode == true) {
                        val newColors = getRandomColors()
                        delay(1000)
                        value = value?.copy(colorList = newColors)
                    }

                } else {
                    value = value?.copy(isRaveMode = false)
                    Log.e(TAG, value.toString())
                }
            }
        }
    }
}